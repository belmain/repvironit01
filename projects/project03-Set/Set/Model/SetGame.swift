//
//  SetGame.swift
//  Set
//
//  Created by belmain on 03/05/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import Foundation

struct SetGame {
    
    private(set) var cardsOnTable = [SetCard]()
    private(set) var cardsSelected = [SetCard]()
    private(set) var cardsTryMatched = [SetCard]()
    private(set) var cardsRemoved = [SetCard]()
    
    private var deck = SetCardDeck()
    var deckCount: Int { return deck.cards.count }
    
    var isSet: Bool? {
        get {
            guard cardsTryMatched.count == 3 else { return nil }
            return SetCard.isSet(cards: cardsTryMatched)
        }
        set {
            if newValue != nil {
                cardsTryMatched = cardsSelected
                cardsSelected.removeAll()
            } else {
                cardsTryMatched.removeAll()
            }
        }
    }
    
    var helps: [[Int]] {
        var helps = [[Int]]()
        if cardsOnTable.count > 2 {
            for one in 0..<cardsOnTable.count {
                for two in (one+1)..<cardsOnTable.count {
                    for three in (two+1)..<cardsOnTable.count {
                        let cards = [cardsOnTable[one], cardsOnTable[two], cardsOnTable[three]]
                        if SetCard.isSet(cards: cards) {
                            helps.append([one, two, three])
                        }
                    }
                }
            }
        }
        guard let itIsSet = isSet, itIsSet else {
            let matchIndices = cardsOnTable.indices(of: cardsTryMatched)
            return helps.map {Set($0)}
                .filter { $0.isDisjoint(with: Set(matchIndices)) }
                .map { Array($0) }
        }
        return helps
    }
    
    init() {
        for _ in 1...Constants.startCards {
            if let card = deck.draw() {
                cardsOnTable += [card]
            }
        }
    }
    
    mutating func chooseCard(at index: Int) {
        let cardChoosen = cardsOnTable[index]
        if !cardsRemoved.contains(cardChoosen) && !cardsTryMatched.contains(cardChoosen) {
            if  isSet != nil {
                if isSet! { replaceOrRemove3Cards() }
                isSet = nil
            }
            if cardsSelected.count == 2, !cardsSelected.contains(cardChoosen) {
                cardsSelected += [cardChoosen]
                isSet = SetCard.isSet(cards: cardsSelected)
            } else {
                cardsSelected.inOut(element: cardChoosen)
            }
        }
    }
    
    private mutating func replaceOrRemove3Cards() {
        if cardsOnTable.count == Constants.startCards, let take3Cards =  take3FromDeck() {
            cardsOnTable.replace(elements: cardsTryMatched, with: take3Cards)
        } else {
            cardsOnTable.remove(elements: cardsTryMatched)
        }
        cardsRemoved += cardsTryMatched
        cardsTryMatched.removeAll()
    }
    
    private mutating func take3FromDeck() -> [SetCard]? {
        var threeCards = [SetCard]()
        for _ in 0...2 {
            if let card = deck.draw() {
                threeCards += [card]
            } else {
                return nil
            }
        }
        return threeCards
    }
    
    mutating func shuffle() {
        cardsOnTable.shuffle()
    }
    
    mutating func deal3() {
        if let deal3Cards = take3FromDeck() {
            cardsOnTable += deal3Cards
        }
    }
    
    private struct Constants {
        static let startCards = 12
    }
}
