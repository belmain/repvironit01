//
//  SetCardDeck.swift
//  Set
//
//  Created by belmain on 03/05/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import Foundation

struct SetCard: Equatable, CustomStringConvertible {
    
    let number: Variant
    let color: Variant
    let shape: Variant
    let fill: Variant
    
    var description: String { return "\(number)-\(color)-\(shape)-\(fill)" }
    
    enum Variant: Int, CaseIterable, CustomStringConvertible {
        case variant1 = 1
        case variant2 = 2
        case variant3 = 3
        
        var description: String {
            return String(self.rawValue)
        }
        var idx: Int {
            return (self.rawValue - 1)
        }
    }
    
    static func isSet(cards: [SetCard]) -> Bool {
        guard cards.count == 3 else { return false }
        
        let one = cards.reduce(0, { $0 + $1.number.rawValue })
        let two = cards.reduce(0, { $0 + $1.color.rawValue })
        let three = cards.reduce(0, { $0 + $1.shape.rawValue })
        let four = cards.reduce(0, { $0 + $1.fill.rawValue })
        
        let sum = [one, two, three, four]
        
        let isSetCards = sum.reduce(true) { (previusVelue, firstValue) -> Bool in
            let check = firstValue % 3
            
            return check == 0 && previusVelue
        }
        
        return isSetCards
    }
}

struct SetCardDeck {
    private(set) var cards = [SetCard]()
    
    init() {
        for number in SetCard.Variant.allCases {
            for color in SetCard.Variant.allCases {
                for shape in SetCard.Variant.allCases {
                    for fill in SetCard.Variant.allCases {
                        cards.append(SetCard(number: number, color: color, shape: shape, fill: fill))
                    }
                }
            }
        }
    }
    
    mutating func draw() -> SetCard? {
        return cards.count > 0 ? cards.remove(at: Int.random(in: 0..<cards.count)) : nil
    }
}
