//
//  CGRect+Ext.swift
//  Set
//
//  Created by Pavel Pats on 5/20/19.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

extension CGRect {
    func zoomed(by scale: CGFloat) -> CGRect {
        let newWidth = width * scale
        let newHeight = height * scale
        return insetBy(dx: (width - newWidth) / 2, dy: (height - newHeight) / 2)
    }
}
