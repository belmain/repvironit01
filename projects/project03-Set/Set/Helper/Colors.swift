//
//  Colors.swift
//  Set
//
//  Created by  belmain on 09/05/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

struct Colors {
    static let help = #colorLiteral(red: 1, green: 0.5212053061, blue: 1, alpha: 1)
    static let selected = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
    static let matched = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
    static var misMatched = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
}
