//
//  SetVC.swift
//  Set
//
//  Created by belmain on 03/05/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

class SetVC: UIViewController {
    
    private var game = SetGame()
    
    private var helper = 0
    
    @IBOutlet weak var dealButton: BorderButton!
    @IBOutlet weak var newButton: BorderButton!
    @IBOutlet weak var showSet: BorderButton!
    @IBOutlet weak var deckCountLabel: UILabel!
    @IBOutlet weak var boardView: BoardView! {
        didSet {
            let swipe = UISwipeGestureRecognizer(target: self,
                                                 action: #selector(deal3))
            swipe.direction = .down
            boardView.addGestureRecognizer(swipe)
            
            let rotate = UIRotationGestureRecognizer(target: self,
                                                     action: #selector(reshuffle))
            boardView.addGestureRecognizer(rotate)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad ()
        
        updateViewFromModel()
    }
    
    @IBAction func new() {
        game = SetGame()
        boardView.cardViews.removeAll()
        updateViewFromModel()
    }
    
    @IBAction func deal3() {
        game.deal3()
        updateViewFromModel()
    }
    
    @IBAction func help() {
        guard  game.helps.count > 0 else { return }
        game.helps[helper].forEach { (idx) in
            boardView.cardViews[idx].help()
        }
    }
    
    private func updateViewFromModel() {
        updateCardViewsFromModel()
        
        deckCountLabel.text = "Deck: \(game.deckCount )"
        
    }
    private func updateCardViewsFromModel() {
        if boardView.cardViews.count - game.cardsOnTable.count > 0 {
            let cardViews = boardView.cardViews[..<game.cardsOnTable.count ]
            boardView.cardViews = Array(cardViews)
        }
        let numberCardViews =  boardView.cardViews.count
        
        for index in game.cardsOnTable.indices {
            let card = game.cardsOnTable[index]
            if  index > (numberCardViews - 1) {
                let cardView = SetCardView()
                updateCardView(cardView, for: card)
                addTapGestureRecognizer(for: cardView)
                boardView.cardViews.append(cardView)
            } else {
                let cardView = boardView.cardViews[index]
                updateCardView(cardView, for: card)
            }
        }
    }
    
    private func addTapGestureRecognizer(for cardView: SetCardView) {
        let tap = UITapGestureRecognizer(
            target: self,
            action: #selector(tapCard(recognizedBy: )))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        cardView.addGestureRecognizer(tap)
    }
    
    @objc
    private func tapCard(recognizedBy recognizer: UITapGestureRecognizer) {
        guard recognizer.state == .ended, let cardView = recognizer.view! as? SetCardView  else {
            updateViewFromModel()
            return
        }
        game.chooseCard(at: boardView.cardViews.firstIndex(of: cardView)!)
        updateViewFromModel()
    }
    
    private func updateCardView(_ cardView: SetCardView, for card: SetCard) {
        cardView.symbolInt =  card.shape.rawValue
        cardView.fillInt = card.fill.rawValue
        cardView.colorInt = card.color.rawValue
        cardView.count =  card.number.rawValue
        cardView.isSelected =  game.cardsSelected.contains(card)
        
        guard let itIsSet = game.isSet else {
            cardView.isMatched = nil
            return
        }
        if game.cardsTryMatched.contains(card) {
            cardView.isMatched = itIsSet
        }
    }
    
    @objc
    func reshuffle(_ sender: UITapGestureRecognizer) {
        guard sender.state == .ended else { return }
        game.shuffle()
        updateViewFromModel()
    }
}
