//
//  UiFont.swift
//  resorchBitbucket
//
//  Created by Pavel Pats on 4/24/19.
//  Copyright © 2019 Pavel Pats. All rights reserved.
//

import UIKit

extension UIFont {
    class func appFontRegular(size: CGFloat) -> UIFont {

        return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.regular)
    }
}

