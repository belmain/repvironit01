//
//  AppStyle.swift
//  resorchBitbucket
//
//  Created by Pavel Pats on 4/25/19.
//  Copyright © 2019 Pavel Pats. All rights reserved.
//

import UIKit

extension UILabel {
    func setCurentStyle() {
        self.font = UIFont.appFontRegular(size: 17)
        self.textAlignment = .center
    }
}
