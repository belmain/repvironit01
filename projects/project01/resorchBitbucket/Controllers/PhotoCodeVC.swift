//
//  PhotoCodeVC.swift
//  resorchBitbucket
//
//  Created by Pavel Pats on 4/16/19.
//  Copyright © 2019 Pavel Pats. All rights reserved.
//

import UIKit
import SnapKit

class PhotoCodeVC: UIViewController {
    
    var myPhoto = UIImageView()
    let firstNameLabel = UILabel()
    let secondNameLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
     
        makePhoto()
        makeLabelName()
        makeLabelLastName()
        
        firstNameLabel.text = "Pats"
        secondNameLabel.text = "Pavel"
    }
    func makePhoto() {
        let image = UIImage(named: "avatarImageView")
        let imageView = UIImageView(image: image)
        imageView.clipsToBounds = true
        
        self.view.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.height.equalTo(250)
            make.top.equalTo(135)
        }
        imageView.layer.cornerRadius = 125
        myPhoto = imageView
    }
    func makeLabelName() {
        firstNameLabel.setCurentStyle()
        self.view.addSubview(firstNameLabel)
        self.view.addSubview(secondNameLabel)
        firstNameLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.center.x)
            make.top.equalTo(myPhoto.snp.bottom).offset(149)
        }
    }
    func makeLabelLastName()  {
        secondNameLabel.textAlignment = .center
        secondNameLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.center.x)
            make.centerY.equalTo(firstNameLabel.snp.bottom).offset(51)
        }
    }
}
