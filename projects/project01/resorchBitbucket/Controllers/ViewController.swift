//
//  ViewController.swift
//  resorchBitbucket
//
//  Created by Pavel Pats on 4/9/19.
//  Copyright © 2019 Pavel Pats. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func tapCodeButton(_ sender: UIButton) {
        let codeVC = PhotoCodeVC()
        self.navigationController?.show(codeVC, sender: self)
    }
}

