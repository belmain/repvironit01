//
//  ViewController.swift
//  ProjectVIT02
//
//  Created by Pavel Pats on 4/19/19.
//  Copyright © 2019 Pavel Pats. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    
    var flipCount = 0 {
        didSet {
            flipCountLabel.text = "Flips: \(flipCount)"
        }
    }
    private var indexTheme = 0 {
        didSet {
            print (indexTheme, keys[indexTheme])
            emojiChoices = emojiThemes[keys [indexTheme]] ?? []
            emoji = [Int: String]()
        }
    }
    private var emojiThemes: [String: [String]] = [
        "Halloween": ["🎃", "🐉", "🐲", "👻", "🐷", "🐔", "🙀", "😈", "👺", "🤡"],
        "Fruits": ["🍏", "🍊", "🍓", "🍉", "🍇", "🍒", "🍌", "🥝", "🍆", "🍑"],
        "Smile": ["😀", "😂", "🤣", "😃", "😄", "😅", "😆", "😉", "😊", "😋"],
        "Animals": ["🐶", "🐭", "🦊", "🦋", "🐢", "🐸", "🐵", "🐞", "🐿", "🐇"],
        "Christmas": ["🎅", "🎉", "🦌", "⛪️", "🌟", "❄️", "⛄️", "🎄", "🎁", "🔔"],
        "Clothes": ["👚", "👕", "👖", "👔", "👗", "👓", "👠", "🎩", "👟", "⛱"]
    ]
    
    private var emojiChoices = [String] ()
    private var keys: [String] {return Array(emojiThemes.keys)}
    private var emoji = [Int: String]()
    
    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var flipCountLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indexTheme = Int.random(in: 0..<emojiThemes.count)
        updateViewFromModel()
    }
    
    @IBAction func newGame(_ sender: UIButton) {
        game.resetGame()
        updateViewFromModel()
        indexTheme = Int.random(in: 0..<emojiThemes.count)
    }
    
    @IBAction func touchCard(_ sender: UIButton) {
        guard let cardNumber = cardButtons.firstIndex(of: sender) else {
            print("chosen card was not in cardButtons")
            return
        }
        game.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    private func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, emojiChoices.count > 0 {
            emoji[card.identifier] = emojiChoices.remove(at: Int.random(in: 0..<emojiChoices.count))
        }
        return emoji[card.identifier] ?? "?"
    }
    
    func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0)  : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }
        scoreLabel.text = "Score: \(game.score)"
        flipCountLabel.text = "Flips: \(game.flipCount)"
    }
}
