//
//  Concetration.swift
//  ProjectVIT02
//
//  Created by Pavel Pats on 4/22/19.
//  Copyright © 2019 Pavel Pats. All rights reserved.
//

import Foundation

class Concentration {
    
    private(set) var cards = [Card]()
    
    private(set) var flipCount = 0
    private(set) var score = 0
    private var seenCards: Set<Int> = []
    
    private struct Points {
        static let matchBonus = 2
        static let missMatchPenalty = 1
    }
    
    private var indexOfOneAndOnlyFaceUpCard: Int? {
        get {
            var foundIndex: Int?
            for index in cards.indices {
                if cards[index].isFaceUp {
                    guard foundIndex == nil else { return nil }
                    foundIndex = index
                }
            }
            return foundIndex
        }
        set {
            for index in cards.indices {
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
    func resetGame() {
        flipCount = 0
        score = 0
        seenCards = []
        for index in cards.indices {
            cards[index].isFaceUp = false
            cards[index].isMatched = false
        }
        cards.shuffle()
    }
    
    func chooseCard(at index: Int) {
        assert(cards.indices.contains(index), "Concentration.chooseCard(at: \(index)) : Choosen index out of range")
        if !cards[index].isMatched {
            flipCount += 1
            guard let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index else {
                indexOfOneAndOnlyFaceUpCard = index
                return
            }
            compareCards(firstIndex: matchIndex, secondIndex: index)
            cards[index].isFaceUp = true
        }
    }
    
    func compareCards(firstIndex: Int, secondIndex: Int) {
        if cards[firstIndex].identifier == cards[secondIndex].identifier {
            cards[firstIndex].isMatched = true
            cards[secondIndex].isMatched = true
            score += Points.matchBonus
        } else {
            if seenCards.contains(secondIndex) {
                score -= Points.missMatchPenalty
            }
            if seenCards.contains(firstIndex) {
                score -= Points.missMatchPenalty
            }
            seenCards.insert(secondIndex)
            seenCards.insert(firstIndex)
        }
    }
    
    init(numberOfPairsOfCards: Int) {
        for _ in 1...numberOfPairsOfCards {
            let card = Card()
            cards += [card, card]
        }
        cards.shuffle()
    }
}
