//
//  AppDelegate.swift
//  ImageGallery
//
//  Created by belmain on 07/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

    func application(_ app: UIApplication, open inputURL: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        guard inputURL.isFileURL else { return false }

        guard let documentBrowserVC = window?.rootViewController as? DocumentBrowserVC else { return false }

        documentBrowserVC.revealDocument(at: inputURL, importIfNeeded: true) { (revealedDocumentURL, error) in
            if let error = error {
                print("Failed to reveal the document at URL \(inputURL) with error: '\(error)'")
                return
            }
            documentBrowserVC.presentDocument(at: revealedDocumentURL!)
        }
        return true
    }
}
