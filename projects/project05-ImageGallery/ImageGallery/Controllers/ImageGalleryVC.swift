//
//  ImageGalleryVC.swift
//  ImageGallery
//
//  Created by belmain on 10/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

class ImageGalleryVC: UICollectionViewController {
    
    struct Constants {
        static let columnCount = 3.0
        static let minWidthRation = CGFloat(0.03)
    }
    
    var imageGallery = ImageGallery()
    var refuseView = RefuseView()
    var document: ImageGalleryDocument?
    
    var boundsCollectionWidth: CGFloat {
        return (collectionView?.bounds.width)!
    }
    
    var gapItems: CGFloat {
        return (flowLayout?.minimumInteritemSpacing)! * CGFloat((Constants.columnCount - 1.0))
    }
    var gapSections: CGFloat {
        return (flowLayout?.sectionInset.right)! * 2.0
    }
    
    var scale: CGFloat = 1 {
        didSet {
            collectionView?.collectionViewLayout.invalidateLayout()
        }
    }
    
    var predefinedWidth: CGFloat {
        let width = floor((boundsCollectionWidth - gapItems - gapSections) / CGFloat(Constants.columnCount)) * scale
        return  min(max(width, boundsCollectionWidth * Constants.minWidthRation), boundsCollectionWidth)
    }
    
    var firstImage: UIImageView? {
        return (self.collectionView!.cellForItem(at: IndexPath(item: 0, section: 0)) as? CollectionViewCell)?.imageGallery
    }
    
    var flowLayout: UICollectionViewFlowLayout? {
        return collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView!.dropDelegate = self
        collectionView!.dragDelegate = self
        collectionView!.dragInteractionEnabled = true
        collectionView?.addGestureRecognizer(UIPinchGestureRecognizer(
            target: self,
            action: #selector(ImageGalleryVC.zoom(_:)))
            
        )
        collectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        document?.open { success in
            if success {
                self.title = self.document?.localizedName
                self.imageGallery = self.document?.imageGallery ?? ImageGallery()
                self.collectionView?.reloadData()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let navBounds = navigationController?.navigationBar.bounds {
            refuseView.frame =  CGRect(
                x: navBounds.width * 0.6,
                y: 0.0,
                width: navBounds.width * 0.4,
                height: navBounds.height)
            let barButton = UIBarButtonItem(customView: refuseView)
            navigationItem.rightBarButtonItem = barButton
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return imageGallery.images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Image Cell", for: indexPath)
        guard let imageCell = cell as? CollectionViewCell else {
            return cell
        }
        imageCell.changeAspectRatio = { [weak self] in
            guard let aspectRatio = self?.imageGallery.images[indexPath.item].aspectRatio,
                aspectRatio < 0.95 || aspectRatio > 1.05 else { return }
            self?.imageGallery.images[indexPath.item].aspectRatio = 1.0
            self?.flowLayout?.invalidateLayout()
        }
        imageCell.imageURL = imageGallery.images[indexPath.item].url
        return cell
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        flowLayout?.invalidateLayout()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier,
            identifier == "Show Image",
            let imageCell = sender as? CollectionViewCell,
            let indexPath = collectionView.indexPath(for: imageCell),
            let imgVc = segue.destination as? ImageVC else { return }
        imgVc.imageURL = imageGallery.images[indexPath.item].url
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        save()
        guard document?.imageGallery != nil, let firstImage = firstImage?.snapshot else {
            dismiss(animated: true) {
                self.document?.close()
            }
            return
        }
        document?.thumbnail = firstImage
        dismiss(animated: true) {
            self.document?.close()
        }
    }
    
    func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        guard let itemCell = collectionView?.cellForItem(at: indexPath) as? CollectionViewCell,
            let image = itemCell.imageGallery.image else {
                return []
        }
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
        dragItem.localObject = indexPath
        return [dragItem]
    }
    
    func save() {
        document?.imageGallery = imageGallery
        if  document?.imageGallery != nil {
            document?.updateChangeCount(.done)
        }
    }
    
    @objc
    func zoom(_ gesture: UIPinchGestureRecognizer) {
        if gesture.state == .changed {
            scale *= gesture.scale
            gesture.scale = 1.0
        }
    }
}

extension ImageGalleryVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = predefinedWidth
        let aspectRation = CGFloat(imageGallery.images[indexPath.item].aspectRatio)
        return CGSize(width: width, height: width / aspectRation)
    }
}

extension ImageGalleryVC: UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        let isSelf = (session.localDragSession?.localContext as?
            UICollectionView) == collectionView
        if isSelf {
            return session.canLoadObjects(ofClass: UIImage.self)
        } else {
            return session.canLoadObjects(ofClass: NSURL.self) &&
                session.canLoadObjects(ofClass: UIImage.self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession,
                        withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy,
                                            intent: .insertAtDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ??
            IndexPath(item: 0, section: 0)
        for item in coordinator.items {
            guard let sourceIndexPath = item.sourceIndexPath else {
                dragFromOtherApp(coordinator: coordinator, item: item, destinationIndexPath: destinationIndexPath)
                return
            }
            let imageInfo = imageGallery.images[sourceIndexPath.item]
            collectionView.performBatchUpdates({
                imageGallery.images.remove(at: sourceIndexPath.item)
                imageGallery.images.insert(imageInfo,
                                           at: destinationIndexPath.item)
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destinationIndexPath])
            })
            coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
        }
    }
    
    func dragFromOtherApp(coordinator: UICollectionViewDropCoordinator, item: UICollectionViewDropItem,
                          destinationIndexPath: IndexPath) {
        let placeholderContext = coordinator.drop(item.dragItem, to: UICollectionViewDropPlaceholder(
            insertionIndexPath: destinationIndexPath, reuseIdentifier: "DropPlaceholderCell"))
        var imageURLLocal: URL?
        var aspectRatioLocal: Double?
        item.dragItem.itemProvider.loadObject(ofClass: UIImage.self) {
            (provider, _) in DispatchQueue.main.async {
                guard let image = provider as? UIImage else { return }
                aspectRatioLocal = Double(image.size.width) / Double(image.size.height)
            }
        }
        item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { (provider, _) in DispatchQueue.main.async {
            guard let url = provider as? URL else { return }
            imageURLLocal = url.imageURL
            guard imageURLLocal != nil, aspectRatioLocal != nil else {
                placeholderContext.deletePlaceholder()
                return
            }
            placeholderContext.commitInsertion(dataSourceUpdates: { insertionIndexPath in
                self.imageGallery.images.insert(ImageModel(url: imageURLLocal!, aspectRatio: aspectRatioLocal!),
                                                at: insertionIndexPath.item) })
            }
        }
    }
}

extension ImageGalleryVC: UICollectionViewDragDelegate {
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession,
                        at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        return dragItems(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession,
                        at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
}
