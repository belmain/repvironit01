//
//  ImageVC.swift
//  ImageGallery
//
//  Created by belmain on 07/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//
// MARK: lecture file - Cassini

import UIKit

class ImageVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 0.05
            scrollView.maximumZoomScale = 5.0
            scrollView.delegate = self
            scrollView.addSubview(imageView)
        }
    }
    
    var imageView  = UIImageView()
    var imageURL: URL? {
        didSet {
            image = nil
            if view.window != nil {
                fetchImage()
            }
        }
    }
    private var autoZoomed = true
    private var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
            imageView.sizeToFit()
            scrollView?.contentSize = imageView.frame.size
            spinner?.stopAnimating()
            autoZoomed = true
            zoomScale()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if imageView.image == nil {
            fetchImage()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        zoomScale()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        autoZoomed = false
    }

    private func fetchImage() {
        autoZoomed = true
        guard let url = imageURL else { return }
        spinner.startAnimating()
        let request = URLRequest(url: url)
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            
            guard let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) else {
                let urlContents = try? Data(contentsOf: url)
                DispatchQueue.main.async {
                    guard let imageData = urlContents, url == self?.imageURL else { return }
                    self?.image = UIImage(data: imageData)
                }
                return
            }
            DispatchQueue.main.async {
                self?.image = image
                self?.spinner?.stopAnimating()
            }
        }
    }
    
    private func zoomScale() {
        if !autoZoomed { return }
        guard let scrollViewU = scrollView else { return }
        if image != nil &&
            (imageView.bounds.size.width > 0) &&
            (scrollView.bounds.size.width > 0) {
            let widthRatio = scrollView.bounds.size.width  / imageView.bounds.size.width
            let heightRatio = scrollView.bounds.size.height / self.imageView.bounds.size.height
            scrollViewU.zoomScale = (widthRatio > heightRatio) ? widthRatio : heightRatio
            scrollViewU.contentOffset = CGPoint(x: (imageView.frame.size.width -
            scrollViewU.frame.size.width) / 2, y: (imageView.frame.size.height - scrollViewU.frame.size.height) / 2)
        }
    }
}
