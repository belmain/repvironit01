//
//  UIImageView+Ext.swift
//  ImageGallery
//
//  Created by belmain on 18/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

extension UIImageView {
    func transition(toImage image: UIImage?) {
        UIView.transition(with: self, duration: 0.3,
                          options: [.transitionCrossDissolve],
                          animations: { self.image = image },
                          completion: nil)
    }
}
