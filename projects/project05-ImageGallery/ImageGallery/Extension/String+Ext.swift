//
//  String+Ext.swift
//  ImageGallery
//
//  Created by belmain on 18/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

extension String {
    func strToImage() -> UIImage? {
        let size = CGSize(width: 130, height: 130)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6).set()
        let rect = CGRect(origin: CGPoint(), size: size)
        UIRectFill(CGRect(origin: CGPoint(), size: size))
        (self as NSString).draw(in: rect,
                                withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25)])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
