//
//  CollectionViewCell.swift
//  ImageGallery
//
//  Created by belmain on 09/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

let cache =  URLCache.shared

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageGallery: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var changeAspectRatio: (() -> Void)?
    var imageURL: URL? {
        didSet {
            updateUI()
        }
    }
    
    private func updateUI() {
        guard let url = imageURL else { return }
        imageGallery.image = nil
        let request = URLRequest(url: url)
        self.spinner?.startAnimating()
        DispatchQueue.global(qos: .userInitiated).async {
            self.sendCard(request: request, url: url)
        }
        spinner?.stopAnimating()
    }
    
    func sendCard(request: URLRequest, url: URL) {
        guard let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) else {
            URLSession.shared.dataTask(with: request, completionHandler: { (data, response, _) in
                guard let data = data, let response = response,
                    ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300,
                    let image = UIImage(data: data), url == self.imageURL else {
                        DispatchQueue.main.async {
                            self.spinner?.stopAnimating()
                            self.imageGallery?.image =
                                "Error".strToImage()
                            self.changeAspectRatio?()
                        }
                        return
                }
                let cachedData = CachedURLResponse(response: response, data: data)
                cache.storeCachedResponse(cachedData, for: request)
                DispatchQueue.main.async {
                    self.imageGallery?.image =  image
                    self.spinner?.stopAnimating()
                }
            }).resume()
            return 
        }
        DispatchQueue.main.async { self.imageGallery?.transition(toImage: image)
            self.spinner?.stopAnimating()
        }
    }
}
