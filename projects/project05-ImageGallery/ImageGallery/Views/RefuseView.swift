//
//  RefuseView.swift
//  ImageGallery
//
//  Created by belmain on 10/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

class RefuseView: UIView, UIDropInteractionDelegate {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        let dropInteraction = UIDropInteraction(delegate: self)
        addInteraction(dropInteraction)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if self.subviews.count > 0 {
            self.subviews[0].frame = CGRect(x: bounds.width - bounds.height,
                                            y: 0, width: bounds.height, height: bounds.height)
        }
    }

    func dropInteraction(_ interaction: UIDropInteraction,
                         canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: UIImage.self)
    }

    func dropInteraction(_ interaction: UIDropInteraction,
                         sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        if session.localDragSession != nil {
            return UIDropProposal(operation: .copy)
        } else {
            return UIDropProposal(operation: .forbidden)
        }
    }

    func dropInteraction(_ interaction: UIDropInteraction, previewForDropping item: UIDragItem,
                         withDefault defaultPreview: UITargetedDragPreview ) -> UITargetedDragPreview? {
        let target = UIDragPreviewTarget(
            container: self,
            center: CGPoint(x: bounds.width - bounds.size.height * 1 / 2,
                            y: bounds.size.height * 1 / 2),
            transform: CGAffineTransform(scaleX: 0.1, y: 0.1))
        return defaultPreview.retargetedPreview(with: target)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction,
                         performDrop session: UIDropSession) {
        session.loadObjects(ofClass: UIImage.self) { _ in
            guard let collection = (session.localDragSession?.localContext as? UICollectionView),
                let images = (collection.dataSource as? ImageGalleryVC)?.imageGallery.images,
                let items = session.localDragSession?.items else { return }
            var indexPaths = [IndexPath]()
            var indexes = [Int]()
            for item in items {
                guard let indexPath = item.localObject as? IndexPath else { return }
                let index = indexPath.item
                indexes += [index]
                indexPaths += [indexPath]
            }
            collection.performBatchUpdates({
                collection.deleteItems(at: indexPaths)
                (collection.dataSource as? ImageGalleryVC)?.imageGallery.images = images.enumerated()
                    .filter { !indexes.contains($0.offset) }
                    .map { $0.element }
            })
        }
    }
}
