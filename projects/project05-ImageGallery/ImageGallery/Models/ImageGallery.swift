//
//  ImageGallery.swift
//  ImageGallery
//
//  Created by belmain on 10/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import Foundation

struct ImageModel: Codable {
    var url: URL
    var aspectRatio: Double
}

struct ImageGallery: Codable {
    var images = [ImageModel]()
    
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init?(json: Data) {
        guard let newValue = try? JSONDecoder().decode(ImageGallery.self, from: json) else { return nil }
        self = newValue
    }
    
    init () {
        self.images = [ImageModel]()
    }
}
