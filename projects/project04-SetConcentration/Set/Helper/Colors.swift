//
//  Colors.swift
//  Set
//
//  Created by  belmain on 09/05/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

struct Colors {
    static let violet = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
    static let red = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
    static let purple = #colorLiteral(red: 1, green: 0.9826831785, blue: 0.2359972586, alpha: 1)
    static let selected = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1).cgColor
    static let matched = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1).cgColor
    static var misMatched = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
    static let help = #colorLiteral(red: 1, green: 0.5212053061, blue: 1, alpha: 1).cgColor
}
