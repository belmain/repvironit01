//
//  ConcentrationVC.swift
//  ProjectVIT02
//
//  Created by Pavel Pats on 4/19/19.
//  Copyright © 2019 Pavel Pats. All rights reserved.
//

import UIKit

class ConcentrationVC: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    
    var flipCount = 0 {
        didSet {
            flipCountLabel.text = "Flips: \(flipCount)"
        }
    }
    private var indexTheme = 0 {
        didSet {
            print (indexTheme)
            guard let emojiThemesU = emojiThemes else { return }
            let oneEmojiThemes = emojiThemesU[indexTheme]
            emojiChoices = oneEmojiThemes.emojis
            emoji = [Int: String]()
        }
    }
    
    var currentEmojiThemes: Int?
    
    var emojiThemes: [EmojiThemes]?
    private var emojiThemesU: [EmojiThemes] {
        get {
            guard let themes = emojiThemes else {
                return [EmojiThemes]()
            }
            return themes
        }
    }
    
    private var emojiChoices = [String] ()
    private var emoji = [Int: String]()
    
    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var flipCountLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indexTheme = currentEmojiThemes ?? 0
        updateSelectedCard()
    }
    
    @IBAction func newGame(_ sender: UIButton) {
        game.resetGame()
        updateSelectedCard()
        indexTheme = Int.random(in: 0..<emojiThemesU.count)
    }
    
    @IBAction func touchCard(_ sender: UIButton) {
        guard let cardNumber = cardButtons.firstIndex(of: sender) else {
            print("chosen card was not in cardButtons")
            return
        }
        game.chooseCard(at: cardNumber)
        updateSelectedCard()
    }
    
    private func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, emojiChoices.count > 0 {
            emoji[card.identifier] = emojiChoices.remove(at: Int.random(in: 0..<emojiChoices.count))
        }
        return emoji[card.identifier] ?? "?"
    }
    
    func updateSelectedCard() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0)  : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }
        scoreLabel.text = "Score: \(game.score)"
        flipCountLabel.text = "Flips: \(game.flipCount)"
    }
}
