//
//  SetVC.swift
//  Set
//
//  Created by Pavel Pats on 5/16/19.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

class SetGameViewController: UIViewController {
    
    private var game = SetGame()
    private var helper = 0
    
    private var deckCenter: CGPoint {
        return view.convert(stackMessage.center, to: boardView)
    }
    
    private var discardPileCenter: CGPoint {
        return
            stackMessage.convert(setsButton.center, to: boardView)
    }
    
    private var matchedSetCardViews: [SetCardView] {
        return  boardView.cardViews.filter { $0.isMatched == true }
    }
    
    private var dealSetCardViews: [SetCardView] {
        return  boardView.cardViews.filter { $0.alpha == 0 }
    }
    
    private var tmpCards = [SetCardView]()
    
    lazy var animator = UIDynamicAnimator(referenceView: view)
    lazy var cardBehavior = CardBehavior(in: animator)
    
    @IBOutlet weak var setsButton: UIButton!
    @IBOutlet weak var newButton: BorderButton!
    @IBOutlet weak var showSet: BorderButton!
    @IBOutlet weak var stackMessage: UIStackView!
    
    @IBOutlet weak var boardView: BoardView! {
        didSet {
            let rotate = UIRotationGestureRecognizer(target: self,
                                                     action: #selector(reshuffle))
            boardView.addGestureRecognizer(rotate)
        }
    }
    
    @IBOutlet weak var deckImageView: DeckImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGestureRecognizer(imageView: deckImageView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateViewFromModel()
    }
    
    @IBAction func new() {
        game = SetGame()
        boardView.resetCards()
        tmpCards.forEach { (tmpCard) in tmpCard.removeFromSuperview() }
        tmpCards = []
        updateViewFromModel()
    }
    
    @IBAction func help() {
        guard  game.helps.count > 0 else { return }
        game.helps[helper].forEach { (idx) in
            boardView.cardViews[idx].help()
        }
    }
    
    private func updateViewFromModel() {
        updateCardViewsFromModel()
        deckImageView.deckNumberString = "\(game.deckCount)"
    }
    
    private func updateCardViewsFromModel() {
        if boardView.cardViews.count - game.cardsOnTable.count > 0 {
            boardView.removeCardViews(removedCardViews: matchedSetCardViews)
        }
        var  newCardViews = [SetCardView]()
        let numberCardViews =  boardView.cardViews.count
        for index in game.cardsOnTable.indices {
            let card = game.cardsOnTable[index]
            if  index > (numberCardViews - 1) {
                let cardView = SetCardView()
                updateCardView(cardView, for: card)
                cardView.alpha = 0
                addTapGestureRecognizer(imageView: cardView)
                newCardViews += [cardView]
            } else {
                let cardView = boardView.cardViews [index]
                if cardView.alpha < 1 && cardView.alpha > 0 &&
                    game.isSet != true {
                    cardView.alpha = 0
                }
                updateCardView(cardView, for: card)
            }
        }
        boardView.addCardViews(newCardViews: newCardViews)
        flyAwayAnimation()
        dealAnimation ()
    }
    
    private func updateCardView(_ cardView: SetCardView, for card: SetCard) {
        cardView.symbolInt = card.shape.rawValue
        cardView.fillInt = card.fill.rawValue
        cardView.colorInt = card.color.rawValue
        cardView.count = card.number.rawValue
        cardView.isSelected = game.cardsSelected.contains(card)
        if let itIsSet = game.isSet {
            if game.cardsTryMatched.contains(card) {
                cardView.isMatched = itIsSet
            }
        } else {
            cardView.isMatched = nil
        }
    }
    
    private func dealAnimation() {
        var currentDealCard = 0
        let timeInterval = 0.15 * Double(boardView.rowsGrid + 1)
        Timer.scheduledTimer(withTimeInterval: timeInterval,
                             repeats: false) { (_) in
            for  cardView in self.dealSetCardViews {
                cardView.animateDeal(from: self.deckCenter,
                                     delay: TimeInterval(currentDealCard) * 0.25)
                currentDealCard += 1
            }
        }
    }
    
    func invisibleCardAnimation() -> [SetCardView] {
        var storageCards = [SetCardView]()
        let alreadyFliedCount = matchedSetCardViews.filter { ($0.alpha < 1 && $0.alpha > 0) }.count
        guard game.isSet != nil, game.isSet!, alreadyFliedCount == 0 else {
            return [SetCardView]()
        }
        storageCards.forEach { tmpCard in
            tmpCard.removeFromSuperview()
        }
        storageCards = []
        matchedSetCardViews.forEach { setCardView in
            setCardView.alpha = 0.2
            guard let cardCopy = setCardView.copy() as? SetCardView else { return }
            storageCards.append(cardCopy)
        }
        storageCards.forEach { tmpCard in
            boardView.addSubview(tmpCard)
            cardBehavior.addItem(tmpCard)
        }
        storageCards[0].addDiscardPile = { [weak self] in
            if let countSets = self?.game.numberSets, countSets > 0 {
                self?.setsButton.alpha = 0
            }
        }
        storageCards[2].addDiscardPile = { [weak self] in
            if let countSets = self?.game.numberSets, countSets > 0 {
                self?.setsButton.alpha = 0
            }
        }
        return storageCards
    }
    
    private func flyAwayAnimation() {
        let storageCards = invisibleCardAnimation()
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (_) in
            var dela = 1
            for tmpCard in storageCards {
                self.cardBehavior.removeItem(tmpCard)
                tmpCard.animateFly(to: CGPoint(x: self.view.frame.width/2, y: -100),
                                   delay: TimeInterval(dela) * 0.25)
                dela += 1
            }
        }
    }
    
    private func addTapGestureRecognizer(imageView: UIView) {
        let tap = UITapGestureRecognizer(
            target: self,
            action: #selector(tapCard(recognizedBy: )))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        imageView.addGestureRecognizer(tap)
    }
    
    @objc
    private func tapCard(recognizedBy recognizer: UITapGestureRecognizer) {
        guard recognizer.state == .ended, let cardView = recognizer.view else {
            updateViewFromModel()
            return
        }
        if let cardViewU = cardView as? SetCardView {
            game.chooseCard(at: boardView.cardViews.firstIndex(of: cardViewU)!)
        } else if cardView is DeckImageView {
            game.deal3()
        }
        updateViewFromModel()
    }
    
    @objc
    func reshuffle(_ sender: UITapGestureRecognizer) {
        guard sender.state == .ended else { return }
        game.shuffle()
        updateViewFromModel()
    }
}
