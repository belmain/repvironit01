//
//  SelectThemeVC.swift
//  Set
//
//  Created by  belmain on 01/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

struct EmojiThemes {
    var title: String
    var emojis: [String]
}

class SelectThemeVC: UIViewController {
    @IBOutlet weak var themeTableView: UITableView!
    
    private var emojiThemes: [EmojiThemes] = {
        let theme0 = EmojiThemes.init(title: "Halloween", emojis: ["🎃", "🐉", "🐲", "👻", "🐷", "🐔", "🙀", "😈", "👺", "🤡"])
        let theme1 = EmojiThemes.init(title: "Fruits", emojis: ["🍏", "🍊", "🍓", "🍉", "🍇", "🍒", "🍌", "🥝", "🍆", "🍑"])
        let theme2 = EmojiThemes.init(title: "Smile", emojis: ["😀", "😂", "🤣", "😃", "😄", "😅", "😆", "😉", "😊", "😋"])
        let theme3 = EmojiThemes.init(title: "Animals", emojis: ["🐶", "🐭", "🦊", "🦋", "🐢", "🐸", "🐵", "🐞", "🐿", "🐇"])
        let theme4 = EmojiThemes.init(title: "Christmas", emojis: ["🎅", "🎉", "🦌", "⛪️", "🌟", "❄️", "⛄️", "🎄", "🎁", "🔔"])
        let theme5 = EmojiThemes.init(title: "Clothes", emojis: ["👚", "👕", "👖", "👔", "👗", "👓", "👠", "🎩", "👟", "⛱"])
        
        let mass = [theme0, theme1, theme2, theme3, theme4, theme5]
        
        return mass
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        themeTableView.dataSource = self
        themeTableView.delegate = self
        themeTableView.reloadData()
        
    }
    
    func moveToConcetration(theme: Int) {
        let valStoryboard = UIStoryboard(name: "SelectTheme", bundle: nil)
        guard let vcl = valStoryboard.instantiateViewController(withIdentifier: "ConcentrationVCStoryboardID") as? ConcentrationVC else { return }
        vcl.currentEmojiThemes = theme
        vcl.emojiThemes = self.emojiThemes
        self.navigationController?.pushViewController(vcl, animated: true)
        navigationController?.navigationBar.barTintColor = UIColor.orange
    }
}

extension SelectThemeVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emojiThemes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = themeTableView.dequeueReusableCell(withIdentifier: "SelectThemeTableViewCellIdentifire", for: indexPath) as? SelectThemeTableViewCell
            else {
                return UITableViewCell()
        }
        let currentTheme = emojiThemes[indexPath.row]
        cell.title.text = currentTheme.title
        let emojiText = currentTheme.emojis.joined(separator: " ")
        cell.emojis.text = emojiText
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension SelectThemeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        moveToConcetration(theme: indexPath.row)
    }
}
