//
//  DeckImageView.swift
//  Set
//
//  Created by Pavel Pats on 5/16/19.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

class DeckImageView: UIImageView {
    
    var deckNumberString: String = "81" {
        didSet {
            setNeedsDisplay()
            setNeedsLayout()
        }
    }
    
    private var numberString: NSAttributedString {
        return centeredAttributedString(deckNumberString, fontSize: labelFontSize)
    }
    
    private lazy var deckNumberLabel = createNumberLabel()
    
    private var labelFontSize: CGFloat {
        let traitCollection = UIScreen.main.traitCollection
        if traitCollection.verticalSizeClass == .regular &&
            traitCollection.horizontalSizeClass  == .regular {
            return bounds.size.height * 0.5
        } else {
            return bounds.size.height * 0.3
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureNumberLabel(deckNumberLabel)
        deckNumberLabel.center = bounds.origin.offsetBy(corX: bounds.size.width/2, corY: bounds.size.height/2)
    }
    
    private func centeredAttributedString(_ string: String, fontSize: CGFloat) -> NSAttributedString {
        var font = UIFont.preferredFont(forTextStyle: .body).withSize(fontSize).bold()
        font = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        return NSAttributedString(string: string, attributes: [.paragraphStyle: paragraphStyle, .font: font])
    }
    
    private func createNumberLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = UIColor.black
        addSubview(label)
        return label
    }
    
    private func configureNumberLabel(_ label: UILabel) {
        label.attributedText = numberString
        label.frame.size = CGSize.zero
        label.sizeToFit()
    }
}
