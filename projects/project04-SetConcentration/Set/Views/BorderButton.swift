//
//  BorderButton.swift
//  Set
//
//  Created by belmain on 03/05/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    private struct DefaultValues {
        static let cornerRadius: CGFloat = 8.0
        static let borderWidth: CGFloat = 4.0
        static let borderColor: UIColor   = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
    }
    
    @IBInspectable var borderColor: UIColor = DefaultValues.borderColor {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = DefaultValues.borderWidth {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = DefaultValues.cornerRadius {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    var disable: Bool {
        get {
            return !isEnabled
        }
        set (newDisabled) {
            isEnabled = (newDisabled ? false : true)
            borderColor = (newDisabled ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0) : DefaultValues.borderColor)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure () {
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        clipsToBounds = true
    }
}
