//
//  SelectThemeTableViewCell.swift
//  Set
//
//  Created by  belmain on 01/06/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

class SelectThemeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var emojis: UILabel!
}
