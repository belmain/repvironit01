//
//  BoardView.swift
//  Set
//
//  Created by Pavel Pats on 5/16/19.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

class BoardView: UIView {
    
    struct Constant {
        static let cellAspectRatio: CGFloat = 0.7
        static let spacingDx: CGFloat  = 3.0
        static let spacingDy: CGFloat  = 3.0
    }
    
    var cardViews = [SetCardView]()
    
    var rowsGrid: Int { return gridCards?.dimensions.rowCount ?? 0 }
    private var gridCards: Grid?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gridCards = Grid(
            layout: .aspectRatio(Constant.cellAspectRatio),
            frame: bounds
        )
        gridCards?.cellCount = cardViews.count
        layoutSetCards()
    }
    
    func layoutSetCards() {
        guard let grid = gridCards else { return }
        let columnsGrid = grid.dimensions.columnCount
        for row in 0..<rowsGrid {
            for column in 0..<columnsGrid where
                cardViews.count > (row * columnsGrid + column) {
                    doAnimationForBoard(row, column, columnsGrid, in: grid)
            }
        }
    }
    
    func doAnimationForBoard(_ row: Int, _ column: Int, _ columnsGrid: Int, in grid: Grid) {
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.3,
            delay: TimeInterval(row) * 0.1,
            options: [.curveEaseInOut],
            animations: {
                guard let val = (grid[row, column]?.insetBy(dx: Constant.spacingDx, dy: Constant.spacingDy)) else { return }
                self.cardViews[row * columnsGrid + column].frame = val
        })
    }
    
    func addCardViews(newCardViews: [SetCardView]) {
        cardViews += newCardViews
        newCardViews.forEach { (setCardView) in
            addSubview(setCardView)
        }
        layoutIfNeeded()
    }
    
    func removeCardViews(removedCardViews: [SetCardView]) {
        removedCardViews.forEach { (setCardView) in
            cardViews = cardViews.filter { $0 != setCardView }
            setCardView.removeFromSuperview()
        }
        layoutIfNeeded()
    }
    
    func resetCards() {
        cardViews.forEach { (_) in
            let cardView = cardViews.popLast()
            cardView?.removeFromSuperview()
        }
        layoutIfNeeded()
    }
}
