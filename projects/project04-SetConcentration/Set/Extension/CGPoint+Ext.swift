//
//  CGPoint+Ext.swift
//  Set
//
//  Created by Pavel Pats on 5/20/19.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

extension CGPoint {
    func offsetBy(corX: CGFloat, corY: CGFloat) -> CGPoint {
        return CGPoint(x: x+corX, y: y+corY)
    }
}
