//
//  Array+Ext.swift
//  Set
//
//  Created by belmain on 03/05/2019.
//  Copyright © 2019 belmain. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    mutating func inOut(element: Element) {
        guard let from = self.firstIndex(of: element) else {
            return self.append(element)
        }
        self.remove(at: from)
    }
    
    mutating func remove(elements: [Element]) {
        self = self.filter { !elements.contains($0) }
    }
    
    mutating func replace(elements: [Element], with new: [Element] ) {
        guard elements.count == new.count else {return}
        for nowIndx in 0..<new.count {
            guard let indexMatched = self.firstIndex(of: elements[nowIndx]) else { continue }
            self[indexMatched] = new[nowIndx]
        }
    }
    
    func indices(of elements: [Element]) -> [Int] {
        guard self.count >= elements.count, elements.count > 0 else { return [] }
        return elements.map { self.firstIndex(of: $0) }.compactMap { $0 }
    }
}
