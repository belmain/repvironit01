//
//  CGFloat+Ext.swift
//  Set
//
//  Created by Pavel Pats on 5/29/19.
//  Copyright © 2019 belmain. All rights reserved.
//

import UIKit

extension CGFloat {
    var random: CGFloat {
        return self * CGFloat(UInt32.random(in: .min ... .max) / UInt32.max)
    }
}
