//
//  Card.swift
//  ProjectVIT02
//
//  Created by Pavel Pats on 4/22/19.
//  Copyright © 2019 Pavel Pats. All rights reserved.
//

import Foundation

struct Card {
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    
    static var identifierFactory = 0
    
    init() {
        self.identifier = Card.getUniguiIdentifier()
    }
    
    static func getUniguiIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
}
